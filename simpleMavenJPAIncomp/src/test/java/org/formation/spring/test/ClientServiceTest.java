package org.formation.spring.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//Hello
//Nouveau coucou David
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class ClientServiceTest {

	@Autowired
	private CrudClientDAO crudClientDAO;

	@Test
	public void testInstCrudClient() {
		assertNotNull(crudClientDAO);		
	}
	@Test
	public void testListClient() {
		Client cl1 = new Client("Hugo","Didier","hugo93","0001",new Adresse(12,"rue dan","Lille"));
		crudClientDAO.save(cl1);		
		//assertTrue(crudClientDAO.findAllClientById("Paul"));;		
	}
}
