package org.formation.spring.test;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.service.IPrestiBanqueService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;

public class Test_TP5 {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);

		IPrestiBanqueService service = context.getBean("service", IPrestiBanqueService.class);

		for(Client c : service.listClients()) {
			System.out.println(c.toString());
		}
		
		Client cl1 = new Client("Hugo", "Didier", "hugo93", "0001", new Adresse(12, "rue dan", "Lille"));
		service.addClient(cl1);

		for(Client c : service.listClients()) {
			System.out.println(c.toString());
		}
		
		((ConfigurableApplicationContext) context).close();

	}

}
