package org.formation.spring.dao;

import java.util.List;

import org.formation.spring.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;



//Lien utile :
// https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.repositories
// https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation

public interface CrudClientDAO extends JpaRepository<Client, Integer>{

	List <Client> findAllClientByNom (String nom);
	List <Client> findAllClientById (String nom);
}
